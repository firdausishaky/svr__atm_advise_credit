FROM node:10-alpine
RUN rm -rf /var/cache/apk/*
     
WORKDIR /app/atm_advise_credit
 
# COPY package.json package.json
COPY . .
 
 
RUN npm install -g pm2 start
RUN apk --update add tzdata
ENV TERM=xterm \
TZ=Asia/Manila

RUN rm -rf /var/cache/apk/*

# COPY . .
 
# EXPOSE 3000
 
# RUN npm install -g nodemon
# RUN echo "bau "
CMD ["sh", "run.sh"]