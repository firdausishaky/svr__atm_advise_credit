/* INFORMATION ==========================
G.OT - Guaranteed overtime is overtime given to some employees with regular working hours in excess of 8 hours per day.

RHOT >= 8Hours (Holiday)
SHOT >= 8Hours (Special Holiday) 
RDOT =  day off an employee
RHRD.OT = day off regular
SHRD.OT = special day off

RH.ND - Night differential hours worked during a regular holiday.
SH.ND - Night differential hours worked during a special holiday.
RD.ND - Night differential hours worked during day off.
RHRD.ND - Night differential hours worked during day off failing on a regular holiday.
SHRD.ND - Night differential hours worked during day off failing on a special holiday.
ND.OT – Night Differential hours worked in excess of regular working hours

Tardiness - (deductions from the salary) the total number of minutes of the employees lates and undertime. Late Policy are as follows
Late = Actual minutes of late.
Undertime = Actual minutes of undertime.


Holiday Rates =====
RH = 100% or 1
SH = 30% or 0.30

Overtime Rates ==============
G.OT 			= 1.25
R.OT 			= 1.25
RH.OT			= 2.60
SH.OT 			= 1.69
RD.OT (1st 8h) 		= 1.30 
RD.OT(excess 8h) 	= 1.69 
RH.RD.OT (1st 8h)		= 2.60 
RH.RD.OT (excess of 8h) 	= 3.38 
SH.RD.OT (1st 8h)		= 1.50 
SH.RD.OT (excess of 8h) 	= 1.95 

sample compute
RH = ( (basic(daily rate) * 0.30) * No.of.Day) + ( (ecola(daily rate) * 0.30) * No.of.Day)
SH = ( (basic(daily rate) * 0.30) * No.of.Day) + ( (ecola(daily rate) * 0.30) * No.of.Day)

ND Rates ==========
R.ND 		= 0.10
NDOT 		= 0.125
RH.ND 		= 0.20
SH.ND 		= 0.13
RD.ND 		= 0.13
RHRD.ND		= 0.26
SHRD.ND 	= 0.15
*/


const holiday_rates = { rh : 1, sh : 0.3 };
var rates = {
		rh 			: 1,
		sh 			: 0.30,
		got 		: 1.25,
		rot 		: 1.25,
		rhot 		: 2.60,
		shot 		: 1.69,
		rotdo_1st 	: 1.30,
		rotdo_excess : 1.69,
		rhrdot_1st 		: 2.60,
		rhrdot_excess 	: 3.38,
		shrdot_1st 		: 1.50,
		shrdot_excess 	: 1.95,

		nd 			: 0.10,
		rnd			: 0.125,
		rhnd 		: 0.20,
		shnd 		: 0.13,
		rdnd		: 0.13,
		rhrdnd 		: 0.26,
		shrdnd 		: 0.15
	};
return module.exports = {
	
	count_rh : (basic, ecola, no_of_day)=>{
		total_basic = (basic * rates.rh) * no_of_day;
		total_ecola = (ecola * rates.rh) * no_of_day;
		total 		= total_basic + total_ecola;

		return {total_basic : total_basic, total_ecola : total_ecola, total : total};
	},

	count_sh : (basic, ecola, no_of_day)=>{
		total_basic = (basic * rates.rh) * no_of_day;
		total_ecola = (ecola * rates.rh) * no_of_day;
		total 		= total_basic + total_ecola;

		return {total_basic : total_basic, total_ecola : total_ecola, total : total};
	},

	count_got : (basic, ecola, got)=>{
		var a = ((got * 2) * (basic * rates.got));
		var b = ((got * 2) * (ecola * rates.got));
		var c = a+b;

		return {total_basic : a, total_ecola : b, total : c};
	},

	count_rot : (basic, ecola, rot)=>{
		var a = (basic * rates.rot) * rot;
		var b = (ecola * rates.rot) * rot ;
		var c = a+b;

		return {total_basic : a, total_ecola : b, total : c};
	},

	// === OT Holiday === 
	count_rhot : (basic, ecola, rhot)=>{
		var a = (basic * rates.rhot) * rhot;
		var b = (ecola * rates.rhot) * rhot ;
		var c = a+b;

		return {total_basic : a, total_ecola : b, total : c};
	},

	count_shot : (basic, ecola, shot)=>{
		var a = (basic * rates.shot) * shot;
		var b =(ecola * rates.shot) * shot ;
		var c = a+b;

		return {total_basic : a, total_ecola : b, total : c};
	},

	count_rotdo_1st : (basic, ecola, rotdo_1st)=>{
		var a = (basic * rates.rotdo_1st) * rotdo_1st;
		var b = (ecola * rates.rotdo_1st) * rotdo_1st;
		var c = a+b;

		return {total_basic : a, total_ecola : b, total : c};
	},

	count_rotdo_excess : (basic, ecola, rotdo_excess)=>{
		var a = (basic * rates.rotdo_excess) * rotdo_excess;
		var b = (ecola * rates.rotdo_excess) * rotdo_excess;
		var c = a+b;

		return {total_basic : a, total_ecola : b, total : c};
	},

	count_rhrdot_1st : (basic, ecola, rhrdot_1st)=>{
		var a = (basic * rates.rhrdot_1st) * rhrdot_1st;
		var b = (ecola * rates.rhrdot_1st) * rhrdot_1st;
		var c = a+b;

		return {total_basic : a, total_ecola : b, total : c};
	},

	count_rhrdot_excess : (basic, ecola, rhrdot_excess)=>{
		var a = (basic * rates.rhrdot_excess) * rhrdot_excess;
		var b = (ecola * rates.rhrdot_excess) * rhrdot_excess ;
		var c = a+b;

		return {total_basic : a, total_ecola : b, total : c};
	},

	count_shrdot_1st : (basic, ecola, shrdot_1st)=>{
		var a = (basic * rates.shrdot_1st) * shrdot_1st;
		var b = (ecola * rates.shrdot_1st) * shrdot_1st;
		var c = a+b;

		return {total_basic : a, total_ecola : b, total : c};
	},

	count_shrdot_excess : (basic, ecola, shrdot_excess)=>{
		var a = (basic * rates.shrdot_excess) * shrdot_excess;
		var b = (ecola * rates.shrdot_excess) * shrdot_excess;
		var c = a+b;

		return {total_basic : a, total_ecola : b, total : c};
	},

	// ==== Night Differential Rate =====

	count_rnd : (basic, ecola, rnd)=>{
		var a = (basic * rates.rnd) * rnd;
		var b = (ecola * rates.rnd) * rnd;
		var c = a+b;

		return {total_basic : a, total_ecola : b, total : c};
	},

	count_rhnd : (basic, ecola, rhnd)=>{
		var a = (basic * rates.rhnd) * rhnd;
		var b = (ecola * rates.rhnd) * rhnd ;
		var c = a+b;

		return {total_basic : a, total_ecola : b, total : c};
	},

	count_shnd : (basic, ecola, shnd)=>{
		var a = (basic * rates.shnd) * shnd;
		var b = (ecola * rates.shnd) * shnd;
		var c = a+b;

		return {total_basic : a, total_ecola : b, total : c};
	},

	count_rhnd : (basic, ecola, rhnd)=>{
		var a = (basic * rates.rhnd) * rhnd;
		var b = (ecola * rates.rhnd) * rhnd;
		var c = a+b;

		return {total_basic : a, total_ecola : b, total : c};
	},

	count_rdnd : (basic, ecola, rdnd)=>{
		var a = (basic * rates.rdnd) * rdnd;
		var b = a + ( (ecola * rates.rdnd) * rdnd );
		return b;
	},

	count_rhrdnd : (basic, ecola, rhrdnd)=>{
		var a = (basic * rates.rhrdnd) * rhrdnd;
		var b = (ecola * rates.rhrdnd) * rhrdnd;
		var c = a+b;

		return {total_basic : a, total_ecola : b, total : c};
	},

	count_shnd : (basic, ecola, shnd)=>{
		var a = (basic * rates.shnd) * shnd;
		var b = (ecola * rates.shnd) * shnd;
		var c = a+b;

		return {total_basic : a, total_ecola : b, total : c};
	},

	count_ndot : (basic, ecola, ndot)=>{
		var a = (basic * rates.ndot) * ndot;
		var b = a + ( (ecola * rates.ndot) * ndot );
		var c = a+b;

		return {total_basic : a, total_ecola : b, total : c};
	},

	count_tardiness : (basic, ecola, tar)=>{
		
		tars = ( tar.late + tar.undertime )
		var a  = basic * tars;
		var b  = ecola * tars;
		var c = a+b;

		return {total_basic : a, total_ecola : b, total : c};
	}

}

/*
Sample Computation
G.OT = No. of GOT (Guaranteed OT) per cutoff for those employees with this OT are computed as
No. of working days 	= 10
Less: Day Off			= (2)
Less: Absences			= (4)
Total					= 4
Multiply by				x 2 hours Guaranteed (OT)
Total Hrs 				= 8

*** R.OT = 2
		| 	Hourly_rate 	| OT Rate 	| No. Of Hrs | Total 	|
Basic 	| 	62.75 			| 1.25 		| 8			 | 627.50 	|
Ecola 	| 	1.25 		 	| 1.25 		| 8 		 | 12.50 	|
Total 	| 											 | 640.00 	|
*/
