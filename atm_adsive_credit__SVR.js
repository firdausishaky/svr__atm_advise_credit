var express = require("express");
var bodyParser = require('body-parser')
var multer  = require('multer')
var cors = require('cors')
var app = express()
//var request = require("request");
//var mysql      = require('mysql');
var mysql      = require('mysql2'); // upgrade
var async = require("async");
const Joi = require('@hapi/joi');

const routes_cfg = require('./libs/route_config');
var libs_res = require('./libs/database_libs');
var Call_procedure = libs_res.Call_procedure;
var result_sql = libs_res.result_sql;
var result_sql2 = libs_res.result_sql2;
var fs = require('fs')



let secret = null;

app.use(cors())

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json())

const db_config = routes_cfg.db_.uat;

const set_ip = routes_cfg.ips.uat;

// var db__LIBS 		= require('./libs/database_libs');
// var Call_procedure 	= db__LIBS.call_procedure;
// var result_sql 		= db__LIBS.result_sql;

// let secret = null;


// app.use(cors())

// // parse application/x-www-form-urlencoded
// app.use(bodyParser.urlencoded({ extended: false }))

// // parse application/json
// app.use(bodyParser.json())


// const db_ = {
// 	local : {	
// 	  /*host     : '10.129.1.16',
// 	  user     : 'isilabs',
// 	  socketPath : '/var/lib/mysql/mysql.sock'*/
// 	  host 		: '127.0.0.1',
// 	  user 		: 'root',
// 	  password 	: '',
// 	  database 	: "payroll",
// 	  waitForConnections: true,
// 	  connectionLimit: 10,
// 	  queueLimit: 0
// 	},
// 	uat : {
// 	  host     : '10.129.1.16',
// 	  user     : 'isilabs',
// 	  //socketPath : '/var/lib/mysql/mysql.sock'
// 	}
// };

// const db_config = db_.local;

// const ips = {
// 	uat 	: "hrmsapi2uat.leekie.com:8080",
// 	local 	: "192.168.1.114:8080", // chang your local ip
// 	my_pc 	: "120.0.01:8080"
// };

// const set_ip = ips.local;

function handleDisconnect() {
	connection = mysql.createConnection(db_config); 

	connection.connect(function(err) {              
		if(err) {                                     
			
			setTimeout(handleDisconnect, 2000); 
		}                                     
		else{
			console.log('connected as id ' + connection.threadId);
		}
	});                                     

	connection.on('error', function(err) {
		
		if(err.code === 'PROTOCOL_CONNECTION_LOST') { 
			handleDisconnect();                         
		} else {                                      
			return console.log(err,"DB");
		}
	});
}

handleDisconnect();

const msg_validate = { header : {"message":"validation error", status:500},data : []};
const msg_empty = { header : {"message":"Data is Empty", status:200}, data : []};
var msg_success = { header : { message : "Request is Success", status:200 }, data : null};


var storage_TMP = multer.diskStorage({
	destination: function (req, file, cb) {
		cb(null, './uploads_TMP/')
	},
	filename: function (req, file, cb) {
		try{
			var type = file.mimetype.split("/")[1].toLowerCase();

			if(type == 'jpeg' || type == 'jpg'){
				var t = file.fieldname + '-' + Date.now()+'.'+type;
				cb(null, t);
			}else if(type == 'png'){
				var t = file.fieldname + '-' + Date.now()+'.'+type;
				cb(null, t);
			}else{ cb('File upload allowed jpg and png only.', false); }
		}catch(e){
			cb('ERR::Fileuploads', false);
		}
	}
})
var upload_TMP = multer({ storage: storage_TMP })


function Movefile(file, dir2, cb){
  //include the fs, path modules
  var fs = require('fs');
  var path = require('path');

  //gets file name and adds it to dir2
  var f = path.basename(file);
  var dest = path.resolve(dir2, f);

  fs.rename(file, dest, (err)=>{
  	if(err) cb(err, false);
  	else cb(false, true);
  });
};








  /*
	* Route ATM ADSIVE CREDIT
	* GET /ID, UPDATE /ID , INSERT, DELETE /ID
	* return Message
	*/
	app.get("/atm_adsive_credit",(req,res)=>{
		
		var obj = {
			local : {
				local_it : 2,
				dropdown : [
				{
					title : "Monthly Local Payroll",
					value : 1
				},{
					title: 'Monthly Incentives',
					value : 2
				},
				{
					title: '13 Month pay & Other bonuses',
					value : 3
				},
				{
					title: 'Service Incentive Leave pay',
					value : 4
				}
				],
				hold_salary : [] // default null
			},
			expat : {
				local_it :1,
				dropdown : [
				{
					title : 'Monthly Expartriate Payroll',
					value : 1
				}
				]
			}
		};
		msg_success.data = obj;
		res.status(200).json(msg_success)
		//result_sql(obj, res);

	});

	app.post("/atm_adsive_credit/update/:id(\\d+)",(req,res)=>{

		async.waterfall([
			(cb)=>{
				const schema = Joi.object().keys({
					id 		: Joi.number(),
					title 	: Joi.string().regex(/^[a-zA-Z0-9 ]{3,30}$/).required(),
					amount 	: Joi.number().required()
				});

				console.log(req.body,1000);
				const valid = Joi.validate(req.body , schema);
				
				if(valid.error != null){
					return res.status(500).json(valid.error);
				}

				cb(false, valid.value);
			},
			(dt, cb)=>{
				Call_procedure('atm_adsive_credit__UPDATE',dt,(e,d)=>{
					connection.execute("call "+d.procedure+"("+d.params_tag+")",d.input, (e,r)=>{
						if(e){ return res.status(500).json(e);}
						else{
							cb(false, r);
						}
					})
				})
			}
			],(e,r)=>{
				result_sql(r[0],res);		
			});
	});

	app.post("/atm_adsive_credit/search",(req,res)=>{

		async.waterfall([
			(cb)=>{
				const schema = Joi.object().keys({
					local_it : Joi.number().required(),
					type : Joi.number().required()
				});

				console.log(req.body,1000);
				const valid = Joi.validate(req.body , schema);
				
				if(valid.error != null){
					return res.status(500).json(valid.error);
				}

				cb(false, valid.value);
			},
			(dt, cb)=>{
				const y = new Date();
				if(dt.local_it != 1){				
					// SEMENTARA statis
					var m = new Date().getMonth();
					//console.log(m)
					// datas = { month : m++, year : y.getFullYear(), type : req.body.type }
					datas = { month : 1, year : 2019, type : req.body.type }
					Call_procedure('atm_search',datas,(e,d)=>{
						connection.execute("call "+d.procedure+"("+d.params_tag+")",d.input, (e,r)=>{
							if(e){ return res.status(500).json(e);}
							else{
								//return res.status(200).json(r[0]);
								// if(r[0].length > 0){
								// 	if(req.body.type == 1){
								// 		var final = [];
								// 		var json = JSON.parse(r[0][0].account);
								// 		for(var i=0; i < json.length; i++){
								// 			const a = { account_number : null, employee_name : json[i].employee_name, employee_id : json[i].employee_id, department_id : json[i].department_id, department_name : json[i].department_name, net_pay : json[i].detail.net_pay};
								// 			final.push(a);
								// 		}
								// 	}
								// 	cb(false, final);
								// }else{
								// 	cb(msg_empty, false);	
								// }
								

								if(r[0].length > 0){
									if(req.body.type == 1){
										var final = [];

										json = []
										newDatas= r[0] 

										for (var a = 0; a < newDatas.length; a++) {

											json.push(JSON.parse(newDatas[a].account))
										}

										for(var i=0; i < json.length; i++){
											for (var j = 0; j < json[i].length; j++) {

												const a = { account_number : null, employee_name : json[i][j].employee_name, employee_id : json[i][j].employee_id, department_id : json[i][j].department_id, department_name : json[i][j].department_name, net_pay : json[i][j].detail.net_pay};
												

												var findIdx = final.findIndex((str)=>{
													return str.employee_id == a.employee_id;
												});

												if (findIdx == -1) {
													final.push(a);

												}else{
													final[findIdx].net_pay = final[findIdx].net_pay + a.net_pay	
												}	

											}	


										}
									}
									cb(false, final);
								}else{
									cb(msg_empty, false);	
								}
							}
						})
					})
				}else{
					Call_procedure('monthly_expatriate',dt,(e,d)=>{
						connection.execute("call "+d.procedure+"("+d.params_tag+")",d.input, (e,r)=>{
							if(e){ return res.status(500).json(e);}
							else{
								if(r[0].length > 0){
									cb(false, r[0]);
								}
							}
						})
					})
				}
			},(dt, cb)=>{
				var final = [];
				async.eachSeries(dt,(key, next)=>{
					async.setImmediate(function () {

						/*if(req.body.type == 1){
							var json = JSON.parse(key.account);
							for(var i=0; i < json.length; i++){
								const a = { employee_name : json[i].employee_name, employee_id : json[i].employee_id, department_id : json[i].department_id, department_name : json[i].department_name, net_pay : json[i].detail.net_pay};
								final.push(a);
							}
						}else if(req.body.type == 2){
								const a = { employee_id : key.employee_id, department_id : key.department_id, department_name : key.department_name, net_pay : key.total};
								final.push(a);
						}else if(req.body.type == 3){
							//console.log("type 3")
							const a = { employee_id : key.employee_id, department_id : key.department_id, department_name : key.department_name, net_pay : key.net_bonus_amount};
							final.push(a);
						}else if(req.body.type == 4){
							const a = { employee_id : key.employee_id, department_id : key.department_id, department_name : key.department_name, net_pay :key.net_payment};
							final.push(a);
						}*/
						Call_procedure('payment_details__GET',{ employee_id : key.employee_id, active : 1 },(e,d)=>{
							connection.execute("call "+d.procedure+"("+d.params_tag+")",d.input, (e,r)=>{
								if(e){}
									else{
										acc_numb = 0;
										if(r[0].length > 0){
											acc_numb = r[0][0].atm_account_no || null;
										}
										if(req.body.type == 1){
											const a = { account_number : acc_numb, employee_name : key.employee_name, employee_id : key.employee_id, department_id : key.department_id, department_name : key.department_name, net_pay : key.net_pay};
											final.push(a);
										// var json = JSON.parse(key.account);
										// for(var i=0; i < json.length; i++){
										// }
									}else if(req.body.type == 2){
										const a = { account_number : acc_numb, employee_id : key.employee_id, department_id : key.department_id, department_name : key.department_name, net_pay : key.total};
										final.push(a);
									}else if(req.body.type == 3){
										const a = {account_number : acc_numb, employee_id : key.employee_id, department_id : key.department_id, department_name : key.department_name, net_pay : key.net_bonus_amount};
										final.push(a);
									}else if(req.body.type == 4){
										//console.log("type 4")
										const a = { account_number : acc_numb,employee_id : key.employee_id, department_id : key.department_id, department_name : key.department_name, net_pay :key.net_payment};
										final.push(a);
									}
									next(null);
								}
							})
						})
						
						//next(null);
					});
				},(err)=>{
					cb(false, final);
				})
			}

			],(e,r)=>{
				result_sql(r,res);		
			});
});



app.post("/atm_adsive_credit/edit",(req,res)=>{

	async.waterfall([
		(cb)=>{
			const account_employee = Joi.object().keys({
				// employee_name : Joi.string().regex(/^[a-zA-Z0-9 ]{3,30}$/).required(),\
				employee_name : Joi.string().allow('',null),
				
				// account_number : Joi.number().required(),
				account_number : Joi.allow('',null),

				net_pay : Joi.number().required(),
				remark : Joi.string().regex(/^[a-zA-Z0-9 ]{3,30}$/),
				hold : Joi.boolean(),
			});
			const schema = Joi.object().keys({
				id 			: Joi.number(),
				type 		: Joi.number().required(),
				local 		: Joi.number().required(),
				created_by 	: Joi.string().regex(/^[a-zA-Z0-9 ]{3,30}$/).required(),
				date 		: Joi.string().required(),
				json_data 	: {
					// bank_name 		: Joi.string().regex(/^[a-zA-Z0-9 ]{3,30}$/).required(),
					// bank_address 	: Joi.string().regex(/^[a-zA-Z0-9 .- (/)]{3,200}$/).required(),
					// re 				: Joi.string().regex(/^[a-zA-Z0-9 .- (/)]{3,30}$/),
					// centavos 		: Joi.string().regex(/^[a-zA-Z0-9 ]{3,100}$/),
					
					bank_name 		: Joi.string().required(),
					bank_address 	: Joi.string().required(),
					re 				: Joi.string(),
					centavos 		: Joi.string(),

					account_employee : Joi.array().items(account_employee),
					grand_total : Joi.number().required(),
					// message : Joi.string().regex(/^[a-zA-Z0-9 ]{3,100}$/),
					message : Joi.string(),

					submit_by : {
						// file : Joi.string().regex(/^[a-zA-Z0-9 ._-]{3,100}$/),
						// name : Joi.string().regex(/^[a-zA-Z0-9 ]{3,100}$/),
						// position : Joi.string().regex(/^[a-zA-Z0-9 ]{3,100}$/)
						file : Joi.string(),
						name : Joi.string(),
						position : Joi.string()
					},
					approve_by : {
						// file : Joi.string().regex(/^[a-zA-Z0-9 ._-]{3,100}$/),
						// name : Joi.string().regex(/^[a-zA-Z0-9 ]{3,100}$/),
						// position : Joi.string().regex(/^[a-zA-Z0-9 ]{3,100}$/)
						file : Joi.string(),
						name : Joi.string(),
						position : Joi.string()
					}
				}
			});


			const valid = Joi.validate(req.body , schema);

			if(valid.error != null){
				return res.status(500).json(valid.error);
			}
			cb(false, valid.value);
		},
		(dt, cb)=>{
				// move file
				async.waterfall([
					(cb1)=>{
						if(!dt.id){ dt.id = 0; }

						if(dt.json_data.submit_by.file){
							Movefile('./uploads_TMP/'+dt.json_data.submit_by.file, './uploads/',(e,r)=>{
								if(e){ cb1('file upload error', false); }
								else{ cb1(false, dt); }
							});
						}else{ cb1(false, dt); }
					},(dt, cb1)=>{
						if(dt.json_data.approve_by.file){
							Movefile('./uploads_TMP/'+dt.json_data.approve_by.file, './uploads/',(e,r)=>{
								if(e){ cb1('file upload error', false); }
								else{ cb1(false, dt); }
							});
						}else{ cb1(false, dt); }
					}
					],(err,dt)=>{

						Call_procedure('atm_adsive_credit__INSERT',dt,(e,d)=>{
					//return console.log(d.input,"============================");
					connection.execute("call "+d.procedure+"("+d.params_tag+")",d.input, (e,r)=>{
						if(e){ return res.status(500).json(e);}
						else{ cb(false, r); }
					})
				})
					})

			}
			],(e,r)=>{
				if(e){ r[0] = e; }
				result_sql(r[0],res);		
			});
});

app.post("/atm_adsive_credit/uploadfile", upload_TMP.single('atm_adsive_credit_file'),(req,res)=>{

	async.waterfall([
		(cb)=>{
			if(req.file){				
				Joi.validate({ atm_adsive_credit_file : req.file.filename, inputfile_by : req.body.inputfile_by } , 
					{ 'atm_adsive_credit_file':Joi.string().required(), inputfile_by : Joi.string().valid('submit_by','approve_by').required() }, (err,input)=>{
						if(err){ return cb(msg_validate, false); }
						else{ cb(false,input); }
					});
			}else{ cb(msg_empty, false); }
		}
		],(e,r)=>{
			if(e){ r[0] = e; }
			result_sql(r,res);		
		})
});

app.get("/atm_adsive_credit_history/delete/:id(\\d+)",(req,res)=>{

	async.waterfall([
		(cb)=>{
			Joi.validate({ id : req.params.id } , { 'id':Joi.number().required() }, (err,input)=>{
				if(err){ return cb(msg_validate, false); }
				else{ cb(false,input); }
			});
		},
		(dt, cb)=>{
			Call_procedure('atm_adsive_credit_history__DELETE',dt,(e,d)=>{

				connection.execute("call "+d.procedure+"("+d.params_tag+")",d.input, (e,r)=>{
					if(e){ return res.status(500).json(e);}
					else{ cb(false, r); }
				})
			})
		}
		],(e,r)=>{
			if(e){ r[0] = e; }
			result_sql(r[0],res);		
		})
});




app.post("/atm_adsive_credit/add_personal",(req,res)=>{

	async.waterfall([
		(cb)=>{
			const schema = Joi.object().keys({
				local_it : Joi.number().required(),
				type : Joi.number().required(),
				employee_id : Joi.string().required()
			});

				// console.log(req.body,1000);
				const valid = Joi.validate(req.body , schema);
				
				if(valid.error != null){
					return res.status(500).json(valid.error);
				}

				cb(false, valid.value);
			},
			(dt, cb)=>{
				// return console.log(dt,"zzzzzzzzzzzzzzzzzzz")
				const y = new Date();
				if(dt.local_it != 1){				
					// SEMENTARA statis
					var m = new Date().getMonth();
					//console.log(m)
					// datas = { month : m++, year : y.getFullYear(), type : req.body.type }
					datas = { month : 1, year : 2019, type : req.body.type }
					Call_procedure('atm_search',datas,(e,d)=>{
						connection.execute("call "+d.procedure+"("+d.params_tag+")",d.input, (e,r)=>{
							if(e){ return res.status(500).json(e);}
							else{


								if(r[0].length > 0){
									if(req.body.type == 1){
										var final = [];

										// var json = JSON.parse(r[0][0].account);
										json = []
										newDatas= r[0] 

										for (var a = 0; a < newDatas.length; a++) {

											json.push(JSON.parse(newDatas[a].account))
										}

										// return res.status(200).json(json[0])

										// return res.status(200).json(json[0][1].employee_name)




										for(var i=0; i < json.length; i++){
											// console.log(json[i])
											// return res.status(200).json(json[0].length)
											for (var j = 0; j < json[i].length; j++) {

												const a = { account_number : null, employee_name : json[i][j].employee_name, employee_id : json[i][j].employee_id, department_id : json[i][j].department_id, department_name : json[i][j].department_name, net_pay : json[i][j].detail.net_pay};
												final.push(a);
											}	

												// return console.log(json[i][0].employee_name)	

												// const a = { account_number : null, employee_name : json[i][j].employee_name, employee_id : json[i][j].employee_id, department_id : json[i][j].department_id, department_name : json[i][j].department_name, net_pay : json[i][j].detail.net_pay};
												// final.push(a);

											}
										}
										cb(false, final);
									}else{
										cb(msg_empty, false);	
									}
								}
							})
					})
				}else{
					Call_procedure('monthly_expatriate',dt,(e,d)=>{
						connection.execute("call "+d.procedure+"("+d.params_tag+")",d.input, (e,r)=>{
							if(e){ return res.status(500).json(e);}
							else{
								if(r[0].length > 0){
									cb(false, r[0]);
								}
							}
						})
					})
				}
			},(dt, cb)=>{
				// console.log(dt,"xxxxxxxxxxx")
				var final = [];
				async.eachSeries(dt,(key, next)=>{
					async.setImmediate(function () {

						Call_procedure('payment_details__GET',{ employee_id : key.employee_id, active : 1 },(e,d)=>{
							connection.execute("call "+d.procedure+"("+d.params_tag+")",d.input, (e,r)=>{
								if(e){}
									else{
										acc_numb = 0;
										if(r[0].length > 0){
											acc_numb = r[0][0].atm_account_no || null;
										}
										if(req.body.type == 1){
											const a = { account_number : acc_numb, employee_name : key.employee_name, employee_id : key.employee_id, department_id : key.department_id, department_name : key.department_name, net_pay : key.net_pay};
											final.push(a);
										// var json = JSON.parse(key.account);
										// for(var i=0; i < json.length; i++){
										// }
									}else if(req.body.type == 2){
										const a = { account_number : acc_numb, employee_id : key.employee_id, department_id : key.department_id, department_name : key.department_name, net_pay : key.total};
										final.push(a);
									}else if(req.body.type == 3){
										const a = {account_number : acc_numb, employee_id : key.employee_id, department_id : key.department_id, department_name : key.department_name, net_pay : key.net_bonus_amount};
										final.push(a);
									}else if(req.body.type == 4){
										//console.log("type 4")
										const a = { account_number : acc_numb,employee_id : key.employee_id, department_id : key.department_id, department_name : key.department_name, net_pay :key.net_payment};
										final.push(a);
									}
									next(null);
								}
							})
						})
						
						//next(null);
					});
				},(err)=>{
					cb(false, final);
				})
			}

			],(e,r)=>{
				result_sql(r,res);		
			});
});




app.post("/atm_adsive_credit/generate_excel",(req,res)=>{
	console.log(req.body.file_name)
	
	var carbone = require('carbone');

	var dt = new Date();
	dt = dt.toJSON().replace(/\-/g,"_");
	dt = dt.replace(/\:/g,"_");


	fileName = 'atm_advise_result_'+dt+'.ods'


	carbone.render('./template_carbone/'+req.body.file_name, req.body, function(err, result){
		if (err) {
			return console.log(err);
		}

		fs.writeFileSync('./result/' +fileName, result);
	});	
	res.status(200).json({data:fileName})


});


/*});  END SQL CONNECTION*/

app.listen(3011, () => {
	console.log("Server ATM_ADSIVE_CREDIT running on port 3011");
});
